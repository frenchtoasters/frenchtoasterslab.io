---
title: Frenchtoasters
comments: false
---

Hey there fellow wonder, it seems you've found the `About Me` page! Well if you like the answer to that question then look no further for here is your answer.

"Just a techie that some times writes long instructions on how to do interesting things." - ***Frenchtoasters***

"Enthusiast of small keyboards" - ***Frenchtoasters***

"Loves to ride bikes where instead of drive" - ***Frenchtoasters***

"Writes lots of code just for fun" - ***Frenchtoasters***
